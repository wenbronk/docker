# /bin/bash

CNAME=$1
if [ ! -n "$1" ]; then 
  echo the param CONTAINER_ID or CONTAINER_NAME is must
  exit 0
fi

CPID=$(docker inspect --format "{{.State.Pid}}" $CNAME)
nsenter --target "$CPID" --mount --uts --ipc --net --pid
